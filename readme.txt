11.08.2021

Blog
1 qism Ro’yxatdan o’tish ( AbstractUser modelidan meroslik olib foydalanuvchini ro’yxatdan utkazish  )
	1 shart : CustomUser model yaratish qoshimcha(manzil va telefon nomer qoshilsin)
	2 shart : Login
	3 shart : Logout

2 qism Blog sayt
	1 shart : Post yaratish (nomi, batafsil malumot, rasmi , kategoriyasi, xududi  db da shu ustunlar bo’lishi kerak)
	2 shart : Ktegoriya
	3 shart : Xududlar

3  qism Asosiy
	1 shart : Asosiy oynada xamma kategoriyalar va xududlar  chiqishi kerak
	2 shart : Kategoriya va xududlar bosilganda  o’sha kategoriyaga tegishli bolgan postlar aloxida oynada chiqishi kerak
	3 shart : Asosiy oynada Xamma postlarning rasmi, nomi va to’liq malumotdan kesilgan xolda azgina malumot va BATAFSIL degan button chiqishi kerak. Button bosilganda o’sha post xaqidagi xamma malumotlar aloxida oynada chiqishi kerak

4 qism Post yaratish
	1 shart : Ro’yxatdan o’tgan foydalanuvchilarga headerda post yaratish degan button chiqishi kerak
	2 shart : Button bosilganda post yaratadigan qismga o’tishi kerak va post yarata olishi kerak

5 qism Qila oladiganlar uchun
	1 shart : Foydalanuvchining shaxsiy oynasi bo’lishi kerak va o’z malumotlarini o’zgartira olishi kerak
	2 shart : Foydalanuvchining shaxsiy oynasida o’zi yaratgan xamma postlari  chiqishi kerak
	3 shart : Foydalanuvchi o’zi yaratgan postlarni o’zgartira olishi va o’chirib yuborishi kerak
