from django.shortcuts import render
from blog.models import Post, Category, Area
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request):
	object_list = Post.objects.filter(status='published').all()
	cats = Category.objects.filter(status='active').all()
	areas = Area.objects.filter(status='active').all()

	paginator = Paginator(object_list, 3)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	if request.user.is_authenticated:
		my_posts = Post.objects.filter(author=request.user.id).all()

	return render(request, 'blog/post/list.html', locals())