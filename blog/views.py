from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Post, Category, Area
from .forms import PostForm, PostEditForm
from django.utils import timezone
import datetime
from django.contrib import messages
from django.utils.text import slugify

def post_list(request, cat=None):

    cat_id = Category.objects.filter(status='active', slug=cat).first()
    area_id = Area.objects.filter(status='active', slug=cat).first()
    if cat_id:
        object_list = Post.objects.filter(status='published', category=cat_id.id).all()
    elif area_id:
        object_list = Post.objects.filter(status='published', area=area_id.id).all()
    else:
        object_list = Post.objects.filter(status='published').all()

    cats = Category.objects.filter(status='active').all()
    areas = Area.objects.filter(status='active').all()

    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    
    if request.user.is_authenticated:
        my_posts = Post.objects.filter(author=request.user.id).all()
    
    return render(request,'blog/post/list.html', locals() )

def post_detail(request, year, month, day, post):

    cats = Category.objects.filter(status='active').all()
    areas = Area.objects.filter(status='active').all()
    if request.user.is_authenticated:
        my_posts = Post.objects.filter(author=request.user.id).all()
    post = get_object_or_404(Post, slug = post, status = 'published',
                publish__year = year, publish__month = month, publish__day = day)

    return render(request, 'blog/post/detail.html',	locals() )


@login_required
def create_post(request):
    blocks = 1
    if request.method == 'POST':
        post_form = PostForm(data = request.POST, files = request.FILES)
        blocks = 2
        if post_form.is_valid():
            new_post = post_form.save(commit=False)
            new_post.slug = slugify(new_post.title)
            new_post.author = request.user
            new_post.save()
            blocks = 3
            return redirect(new_post.get_absolute_url())
    else:
        post_form = PostForm()
    
    cats = Category.objects.filter(status='active').all()
    areas = Area.objects.filter(status='active').all()
    my_posts = Post.objects.filter(author=request.user.id).all()
    
    return render(request, 'blog/post/create_post.html', locals())

@login_required
def edit_post(request, post_id):
    postt = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        post_form = PostEditForm(data=request.POST, files = request.FILES)
        if post_form.is_valid():
            new_post = post_form.save(commit=False)
            new_post.slug = slugify(new_post.title)
            new_post.author = request.user
            new_post.updated = datetime.datetime.now()
            new_post.save()
            messages.success(request, 'O\'zgartirish muvaffaqiyatli saqlandi')
            return redirect(new_post.get_absolute_url())
        else:
            messages.error(request, 'O\'zgartirishda xatolik sodir bo\'ldi')
    else:
        post_form = PostEditForm(instance=postt)

    cats = Category.objects.filter(status='active').all()
    areas = Area.objects.filter(status='active').all()
    my_posts = Post.objects.filter(author=request.user.id).all()

    return render(request,'blog/post/edit_post.html', locals())


@login_required
def delete_post(request, post_id):
    
    Post.objects.filter(pk=post_id).delete()

    return redirect('index:index')