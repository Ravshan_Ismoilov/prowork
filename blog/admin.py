from django.contrib import admin
from .models import Post, Category, Area

@admin.register(Category)
@admin.register(Area)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'publish', 'status')
    list_filter = ('status', 'created', 'publish')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}
    date_hierarchy = 'publish'
    ordering = ('status', 'publish')

admin.site.register(Post, PostAdmin)