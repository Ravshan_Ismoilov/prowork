from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    
    path('<int:year>/<int:month>/<int:day>/<slug:post>/', views.post_detail, name='post_detail'),
    path('new_post/', views.create_post, name='create_post'),
    path('edit_post/<int:post_id>', views.edit_post, name='edit_post'),
    path('delete_post/<int:post_id>', views.delete_post, name='delete_post'),
    path('<slug:cat>/', views.post_list, name='post_list'),
    path('', views.post_list, name='post_list'),
]
