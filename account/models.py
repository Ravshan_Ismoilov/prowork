from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=250)
    address = models.CharField(max_length=250)

    def __str__(self):
        return '{} ning shaxsiy profili'.format(self.user.username)