from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm
from .models import Profile
from blog.models import Post, Category, Area


@login_required
def dashboard(request):
    
    users = User.objects.filter(pk=request.user.id).first()
    profile = Profile.objects.filter(user=request.user).first()

    cats = Category.objects.filter(status='active').all()
    areas = Area.objects.filter(status='active').all()
    my_posts = Post.objects.filter(author=request.user.id).all()

    return render(request, 'account/dashboard.html', locals())

@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Shaxsiy profilingiz yangilandi.')
            return redirect('dashboard')
        else:
            messages.error(request, 'Shaxsiy profilingizni yangilashda xatolik sodir bo\'ldi.')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,'account/edit.html', locals())

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request,
                username=cd['username'],
                password=cd['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponse('Ro\'yxatdan muvaffaqiyatli o\'tdingiz.')
            else:
                return HttpResponse('Sizning accountingiz bloklangan!!!')
        else:
            return HttpResponse('Noto\'g\'ri malumot kiritildi.')
    else:
        form = LoginForm()
        
    return render(request, 'account/login.html', locals())


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            Profile.objects.create(user=new_user)
            return render(request,'account/register_done.html', locals() )
    else:
        user_form = UserRegistrationForm()
    return render(request,'account/register.html', locals())